---
layout: markdown_page
title: "CI/CD Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CI/CD Team
{: #cicd}

The CI/CD Team is focused on all the functionality with respect to
Continuous Integration and Deployments.

This team maps to [Verify](/handbook/product/categories/#verify), [Package](/handbook/product/categories/#package), [Release](/handbook/product/categories/#release), and [Configure](/handbook/product/categories/#configure).
