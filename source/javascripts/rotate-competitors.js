(function() {
  var competitorsContainers = document.getElementsByClassName('competitors-container');

  function getLogos() {
    for (var i = 0; i < competitorsContainers.length; i++) {
      if (competitorsContainers[i].children.length > 1) {
        var tlLeft = new TimelineMax({ repeat: -1 });
        tlLeft.staggerTo(competitorsContainers[i].children, 0, {ease: Power4.easeOut, opacity: 1, zIndex: 1}, 4);
        tlLeft.staggerTo(competitorsContainers[i].children, 0, {ease: Power4.easeOut, opacity: 0, zIndex: 0}, 4);
        tlLeft.play();
      } else if (competitorsContainers[i].children.length) {
        competitorsContainers[i].firstElementChild.style.opacity = '1';
      }
    }
  }

  window.addEventListener('load', getLogos);
})();
