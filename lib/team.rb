require 'yaml'

module Gitlab
  module Homepage
    class Team
      def members
        @members ||= Team::Member.all! do |member|
          Team::Project.all! do |project|
            member.assign(project) if member.involved?(project)
          end
        end
      end

      def projects
        @projects ||= Team::Project.all! do |project|
          Team::Member.all! do |member|
            project.assign(member) if member.involved?(project)
          end
        end
      end

      def departments
        @departments ||= begin
          Hash.new(0).tap do |departments|
            Team::Member.all! do |member|
              member.departments.each { |department| departments[department] += 1 }
            end
          end
        end
      end

      def countries
        @countries ||= begin
          members_by_country = Team::Member.all!.group_by(&:country)

          members_by_country.delete('Remote')
          members_by_country.delete(nil)

          members_by_country
            .map { |name, members| { name: name, count: members.count, info: members.first.country_info } }
            .sort_by { |c| c[:info]&.name || c[:name] }
        end
      end
    end
  end
end
